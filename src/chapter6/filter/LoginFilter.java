package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


//ユーザー編集、つぶやき編集の2ページを指定
@WebFilter(urlPatterns={"/setting","/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

        if(session.getAttribute("loginUser") != null){
            // セッションがnullでなければ、通常どおりの遷移
            chain.doFilter(request, response);
        }else{
            // セッションがnullならば、ログイン画面へ飛ばす
        	//エラーメッセージをセットする、リダイレクトする
        	List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインしてください");
            session.setAttribute("errorMessages", errorMessages);
            HttpServletResponse res = (HttpServletResponse)response;
            res.sendRedirect("./login");
        }

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
