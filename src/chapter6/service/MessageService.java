package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            if (!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            //String start
            String defaultStart = "2020-01-01 00:00:00";

            //現在日時の指定
            Date nowDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String defaultEnd = format.format(nowDate);

            String startDate = null;
            String endDate = null;

            //start とendが入っていなかったらデフォルトを使用する
            //入っていたらそのまま使う
            //if (start)
            if (!StringUtils.isEmpty(start)) {
            	startDate = start + " 00:00:00";
            } else {
            	startDate = defaultStart;
            }

            //if (end)
            if (!StringUtils.isEmpty(end)) {
            	endDate = end + " 23:59:59";
            } else {
            	endDate = defaultEnd;
            }

            List<UserMessage> messages =
            		new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);

            commit(connection);
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//deleteメソッドを新規追加
    public void delete(String messageId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = Integer.parseInt(messageId);
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

    //getTextメソッドを新規追加
    public Message select(int id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, id);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

    public void updateMessage(String messageId, String updateMessage) {

    	Connection connection = null;
    	Integer id = null;
		try {
			connection = getConnection();

			if (!messageId.isEmpty()) {
		    	id = Integer.parseInt(messageId);
		    	new MessageDao().updateMessage(connection, id, updateMessage);
		    }

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

}