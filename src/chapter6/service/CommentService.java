package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import chapter6.beans.Message;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

/**
 * Servlet implementation class CommentService
 */
public class CommentService extends HttpServlet {

	public void comment(Message message) {

		 Connection connection = null;
	     try {
	    	 connection = getConnection();
	    	 new CommentDao().comment(connection, message);
	         commit(connection);
	     } catch (RuntimeException e) {
	         rollback(connection);
	         throw e;
	     } catch (Error e) {
	         rollback(connection);
	         throw e;
	     } finally {
	         close(connection);
	     }
	}

	public List<UserComment> replyComment() {

		final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            List<UserComment> comments = new UserCommentDao().replyComment(connection, LIMIT_NUM);
            commit(connection);

            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }


	}
}
