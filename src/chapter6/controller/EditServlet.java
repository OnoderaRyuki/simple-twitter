package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		//メッセージのIDを取得する
	    String messageId = request.getParameter("messageId");

	    Message message = null;

	    //messageIdが空、文字、
	    if (!messageId.isEmpty() && messageId.matches("^[0-9]*$")) {
	    	Integer id = Integer.parseInt(messageId);
	    	message = new MessageService().select(id);
	    }

	    if(message == null) {
	    	errorMessages.add("不正なパラメータが入力されました");
	    	session.setAttribute("errorMessages", errorMessages);
	    	response.sendRedirect("./");
	    	return;
	    }

	    request.setAttribute("message", message);
	    request.getRequestDispatcher("/edit.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

        List<String> errorMessages = new ArrayList<String>();

		String updateMessage = request.getParameter("updateText");

        if (isValid(updateMessage, errorMessages)) {
            try {
            	String messageId = request.getParameter("messageId");
            	new MessageService().updateMessage(messageId, updateMessage);
            	response.sendRedirect("./");
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        } else {
            Message message = getMessage(request);
        	request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("/edit.jsp").forward(request, response);
            return;
        }

	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

        Message message = new Message();
        message.setText(request.getParameter("updateText"));
        return message;
    }

	private boolean isValid(String updateMessage, List<String> errorMessages) {

	    if (StringUtils.isBlank(updateMessage)) {
	        errorMessages.add("メッセージを入力してください");
	    } else if (140 < updateMessage.length()) {
	        errorMessages.add("140文字以下で入力してください");
	    }
	    if (errorMessages.size() != 0) {
	        return false;
	    }
	    return true;
	}

}
