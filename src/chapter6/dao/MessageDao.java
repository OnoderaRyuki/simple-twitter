package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

  public void insert(Connection connection, Message message) {

    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

      ps = connection.prepareStatement(sql.toString());

      ps.setInt(1, message.getUserId());
      ps.setString(2, message.getText());

      ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  //deleteメソッドを新規追加
  public void delete(Connection connection, Integer id) {

	  PreparedStatement ps = null;
	  try {
		  String sql = "DELETE FROM messages WHERE id = ?";
		  ps = connection.prepareStatement(sql);

		  //psにバインド変数を入れる
		  ps.setInt(1, id);
		  ps.executeUpdate();

  	  } catch (SQLException e) {
  		  throw new SQLRuntimeException(e);
  	  } finally {
  	    close(ps);
  	  }

  }

  //getTextメソッドを新規追加
  public Message select(Connection connection, Integer id) {

	  PreparedStatement ps = null;
	  try {
		  String sql = "SELECT * FROM messages WHERE id = ?";
		  ps = connection.prepareStatement(sql);

		  //psにバインド変数を入れる
		  ps.setInt(1, id);
		  ResultSet rs = ps.executeQuery();

		  List<Message> message = toMessage(rs);
          if (message.isEmpty()) {
              return null;
          } else {
              return message.get(0);
          }

	  } catch (SQLException e) {
          throw new SQLRuntimeException(e);
      } finally {
          close(ps);
      }
  }

  private List<Message> toMessage(ResultSet rs) throws SQLException {

      List<Message> messages = new ArrayList<Message>();
      try {
          while (rs.next()) {
              Message message = new Message();
              message.setId(rs.getInt("id"));
              message.setText(rs.getString("text"));

              messages.add(message);
          }
          return messages;
      } finally {
          close(rs);
      }
  }

  public void updateMessage(Connection connection, int id, String updateMessage) {

	  PreparedStatement ps = null;
	  try {
		  String sql = "UPDATE messages SET text = ?, updated_date = CURRENT_TIMESTAMP WHERE id = ?";
		  ps = connection.prepareStatement(sql);

		  //psにバインド変数を入れる
		  ps.setString(1, updateMessage);
		  ps.setInt(2, id);
		  ps.executeUpdate();

  	  } catch (SQLException e) {
  		  throw new SQLRuntimeException(e);
  	  } finally {
  	    close(ps);
  	  }

  }

}