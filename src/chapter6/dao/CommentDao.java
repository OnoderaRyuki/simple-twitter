package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

/**
 * Servlet implementation class CommentDao
 */
public class CommentDao extends HttpServlet {

	public void comment(Connection connection, Message message) {

		PreparedStatement ps = null;
	    try {
	      StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO comments ( ");
	            sql.append("    user_id, ");
	            sql.append("    text, ");
	            sql.append("    message_id, ");
	            sql.append("    created_date, ");
	            sql.append("    updated_date ");
	            sql.append(") VALUES ( ");
	            sql.append("    ?, ");                  // user_id
	            sql.append("    ?, ");                  // text
	            sql.append("    ?, ");					// message_id
	            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
	            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
	            sql.append(")");

	      ps = connection.prepareStatement(sql.toString());

	      ps.setInt(1, message.getUserId());
	      ps.setString(2, message.getText());
	      ps.setInt(3, message.getId());

	      ps.executeUpdate();

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}
